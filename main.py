import asyncio
from datetime import datetime
from playwright.async_api import Playwright, async_playwright
import os
from datetime import datetime
import random

username = os.environ["CRAWLER_USERNAME"]
crawler_password = os.environ["CRAWLER_PASSWORD"]
hours_per_day = int(os.environ['HOURS_PER_DAY'])

list_profile = {124353637,21435367586}

#functions ramdon timeouts
def get_crawling_time(hours_per_day):
    number_crawl_per_day = int(24/hours_per_day)
    started_hour = random.randint(0, 23)
    crawling_hours = [str(started_hour)]
    for i in range(1, number_crawl_per_day):
        if started_hour >= 12:
            next_hour = started_hour - hours_per_day
        else:
            next_hour = started_hour + hours_per_day
        crawling_hours.append(str(next_hour))
    crawling_hours = ",".join(crawling_hours)
    at_minute = str(random.randint(0, 59))
    return crawling_hours, at_minute

async def run(playwright: Playwright) -> None:
    browser = await playwright.chromium.launch(headless=False,slow_mo=1000)
    context = await browser.new_context(http_credentials={'username': username, 'password': crawler_password})
    # Open new page
    page = await context.new_page()
    # Go to about:blank
    await page.goto("about:blank")
    await page.goto("https://sl-sa.ihouzz.com/1/tasks/")
    for profile in list_profile:
        config = f"-d link={profile}\n-d all = 1"
        print(config)
        created_at = datetime.now()
        task_name = f"ads_{profile}_{created_at}"
        print(task_name)
        # Click a:has-text("+")
        await page.locator("a:has-text(\"+\")").click()
        await page.wait_for_url("https://sl-sa.ihouzz.com/1/schedule/?add_task=True")
        # Click text=project No projects found >> i
        await page.locator("text=project No projects found >> i").click()
        # Click li:has-text("neo4jscrapy")
        await page.locator("li:has-text(\"neo4jscrapy\")").click()
        # Click [placeholder="Select a version"]
        await page.locator("[placeholder=\"Select a version\"]").click()
        # Click li:has-text("default: the latest version") >> nth=2
        await page.locator("li:has-text(\"default: the latest version\")").nth(2).click()
        # Click [placeholder="Select a spider"]
        await page.locator("[placeholder=\"Select a spider\"]").click()
        # Click li:has-text("profile") >> nth=1
        await page.locator("li:has-text(\"profile\")").nth(1).click()
        # Click .el-switch__core >> nth=0
        await page.locator(".el-switch__core").first.click()
        # Click text=jobid USER_AGENTMozilla/5.0 Mozilla/5.0 Windows NT Chrome... Mozilla/5.0 iPhone  >> textarea
        await page.locator("text=jobid USER_AGENTMozilla/5.0 Mozilla/5.0 Windows NT Chrome... Mozilla/5.0 iPhone  >> textarea").click()
        # Fill text=jobid USER_AGENTMozilla/5.0 Mozilla/5.0 Windows NT Chrome... Mozilla/5.0 iPhone  >> textarea
        await page.locator("text=jobid USER_AGENTMozilla/5.0 Mozilla/5.0 Windows NT Chrome... Mozilla/5.0 iPhone  >> textarea").fill(config)
        # Click text=actionAdd Task Add Task & Fire Right Now Add Task & Pause It >> i
        await page.locator("text=actionAdd Task Add Task & Fire Right Now Add Task & Pause It >> i").click()
        # Click li:has-text("Add Task") >> nth=0
        await page.locator("li:has-text(\"Add Task\")").first.click()
        # Click [placeholder="textual description of the task \(optional\)"]
        await page.locator("[placeholder=\"textual description of the task \\(optional\\)\"]").click()
        # Fill [placeholder="textual description of the task \(optional\)"]
        await page.locator("[placeholder=\"textual description of the task \\(optional\\)\"]").fill(task_name)
        crawling_hours,at_minute = get_crawling_time(hours_per_day)
        # Click [placeholder="hour \(0-23\)\; 9\,17\,8-20\/4 equals to 8\,9\,12\,16\,17\,20"]
        await page.locator("[placeholder=\"hour \\(0-23\\)\\; 9\\,17\\,8-20\\/4 equals to 8\\,9\\,12\\,16\\,17\\,20\"]").click()
        # Fill [placeholder="hour \(0-23\)\; 9\,17\,8-20\/4 equals to 8\,9\,12\,16\,17\,20"]
        await page.locator("[placeholder=\"hour \\(0-23\\)\\; 9\\,17\\,8-20\\/4 equals to 8\\,9\\,12\\,16\\,17\\,20\"]").fill(crawling_hours)
        print(f"{crawling_hours} is hour ramdon")
        # Click [placeholder="minute \(0-59\)\; defaults to 0\, type \*\/10 to fire every 10 minutes"]
        await page.locator("[placeholder=\"minute \\(0-59\\)\\; defaults to 0\\, type \\*\\/10 to fire every 10 minutes\"]").click()
        # Fill [placeholder="minute \(0-59\)\; defaults to 0\, type \*\/10 to fire every 10 minutes"]
        await page.locator("[placeholder=\"minute \\(0-59\\)\\; defaults to 0\\, type \\*\\/10 to fire every 10 minutes\"]").fill(at_minute)
        print(f"{at_minute} is minute ramdon")
        # Click text=Check CMD
        await page.locator("text=Check CMD").click()

        await page.locator("#run_spider").click()
        print(f"Task name has been added is :{profile}")
    # ---------------------


async def main() -> None:
    async with async_playwright() as playwright:
        print("Được rồi đi thôiiiiiiiiiiiiiiiiiiiiiiii............!")
        await run(playwright)
        print("Finish............!")
asyncio.run(main())